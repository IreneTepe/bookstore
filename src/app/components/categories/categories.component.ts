import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  product: any = [
    {name: 'Azax', price: '30'},
    {name: 'Skip', price: '20'},
    {name: 'Lenor', price: '10'},
    { name: 'Ava', price: '5' },
    { name: 'Ariel', price: '55' },
    { name: 'klinex', price: '59' }
  ];
  changeIndex = [];
  changeValue(index) {
    this.changeIndex.push(index);
    console.log(this.changeIndex);
  }
  constructor() { }

  ngOnInit() {
  }

}
